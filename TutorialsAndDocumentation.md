# Docker
1. Getting Started: https://docs.docker.com/get-started/
2. Containerized Bioinformatics: https://www.melbournebioinformatics.org.au/tutorials/tutorials/docker/media/index.html#1
3. Docker for Bioinformatics: https://bioinformatics-core-shared-training.github.io/docker-4-bioinformatics/
4. Bioinformatics in the Cloud: https://bioinformatics-in-the-cloud-workshop.readthedocs.io/en/latest/01__app_deployment/docker_intro.html
5. Beginning Biologist: https://static-bcrf.biochem.wisc.edu/tutorials/docker/Docker_03.html
