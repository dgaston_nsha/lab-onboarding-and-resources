# Lab Onboarding and Resources

This repository hosts best practices, SOPs, templates, and resources for members of the Gaston Lab along with our affiliates and collaborators. It will also hold training and lecture materials including slide decks and notes.

## Ten Simple Rules
Inspired by the 10 Simple Rules for X series from PLoS Computational Biology, these are ten simple things we can do to work at our best in Bioinformatics and Genomics work:
1. When in Doubt, Ask
2. Be a Good Steward of Data
3. Label Everything
4. Keep Copious Notes
5. Write/Document As You Go
6. Be Curious
7. Never Be Afraid to Take the Time to Learn
8. Investing Time Upfront to Build Tools Always Pays Off
9. Communicate Lots
10. Interact With Empathy and Kindness

## My Basic Philosophy
As a supervisor, I try and foster a great deal of independence. I am always willing to help, to talk, to teach, and to explain but I rely on trainees to come to me and communicate their needs as much as possible. My personal style and preference is to be fairly hands off where I can, and I am most definitely not a micro-manager. But I want to give you the tools you need to manage your own time and ensure you make progress on projects.

Be sure to give yourself plenty of time. Rome wasn't built in a day, and most of our work as Bioinformaticians takes time to put together. Also, you need time for yourself. Take breaks. Protect your mental resources, and do things you enjoy outside of science. This isn't the sum total of our lives, and neither should we be bound to ideas about always trying to be at peak productivity. You need unstructured time to rest your mind, to think creatively, and to take care of your own well being fire and foremost. The work should never come before our own health: physical or mental.

## Basic Project Management
When you start a new project, create a directory on your computer and a space in any sort of documentation tools you use (such as OneNote, EverNote, etc). All the pertinent data, documentation, and code needs to live or be linked here in some fashion. Write lots of notes (See Rule 4) as you go (Rule 5). It will be easier to write your methods, and even introductions and discussions for a thesis or paper if you do this. Document not just the tools, parameters, and results but also your thought process. Why did you do something a certain way? Why this experiment in the first place? What are your rough ideas about what the results mean?

While I despise the cult of productivity, some productivity tools can be useful when we use them for our own benefit. Tools like Trello can let you easily implement lists for things to do, what you are doing, and what you have completed. Toggl, a time tracking tool, can let you see how much of your time different projects or tasks take. Maybe some things take far too much of your time and we can develop ways of them taking less for instance. I find tools like this best used for our own personal benefit and not with the intention of maxzimizing some nebulous idea of our productivity.
